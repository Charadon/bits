/*
Bits
Copyright (C) 2020  Charadon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "bits.h"

int16_t keypress;
int8_t keypressed;
int8_t gameGoing = 0;
int8_t inMenu = 0;
uint8_t debugMode = 0;

int64_t bits = 0;
uint64_t spacebars = 0;
uint64_t programmers = 0;
uint64_t bitfarms = 0;
uint64_t bitmines = 0;
uint64_t bitquant = 0;
uint64_t bitbang = 0;
uint64_t bitharmonizer = 0;
uint64_t bitgod = 0;
uint64_t bitcollider = 0;

sqlite3 *db;
char datapath[200];
//Time
/* Clock           H:M:S */
uint64_t time[] = {0,0,0};
/* Slot            0 1 2 */
uint64_t ticks = 0;
//Save Related


#define BITCOLOR 1
#define UPGRADECOLOR 2
#define CLOCKCOLOR 3
#define REDCOLOR 4

int main(){
	#ifdef USING_UNIX
		sprintf( datapath, "%s/.local/share/bits/bits.sav",  getenv( "HOME" ) );
	#endif
	#ifdef USING_WINDOWS
		sprintf( datapath, "%s/bits/bits.sav",  getenv( "APPDATA" ) );
	#endif
	initscr();
	nodelay(stdscr, TRUE);
	raw();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);
	start_color();
	initColors();
	if (debugMode == 1){
		spacebars = 999999;
		programmers = 999999;
		bitfarms = 999999;
		bitmines = 999999;
		bitquant = 999999;
		bitbang = 999999;
		bitharmonizer = 999999;
		bitgod = 999999;
		bitcollider = 999999;
	}
	titlescreen();
	return 0;
}



void saveFile(){
	clear();
	sqlite3 *db;
	mvwprintw( stdscr, 0, 0, "Creating save file..." );
	refresh();
	char *sql;
	char *errormsg;
	int failed = 0;
	mvwprintw( stdscr, 1, 0, "Deleting old save file..." );
	refresh();
	#ifdef USING_UNIX
		sprintf( datapath, "%s/.local/share/bits", getenv( "HOME" ) );
		struct stat exists = {0};
		if ( stat( datapath, &exists ) == -1 )
			mkdir( datapath, 0700 );
		sprintf( datapath, "%s/.local/share/bits/bits.sav",  getenv( "HOME" ) );
		remove( datapath ); //Deletes old save database
	#endif
	#ifdef USING_WINDOWS
		sprintf( datapath, "%s/bits", getenv( "APPDATA" ) );
		struct stat exists = {0};
		if ( stat( datapath, &exists ) == -1 )
			mkdir( datapath );
		sprintf( datapath, "%s/bits/bits.sav",  getenv( "APPDATA" ) );
		remove( datapath ); //Deletes old save database
	#endif
	failed = sqlite3_open( datapath, &db ); //Creates new database
	if ( failed ) { //Checks if it was successful at creating database.
		mvwprintw( stdscr, 2, 0, "Failed to create save file, do you have write privledges in the directory the game is ran from?" );
		nodelay( stdscr, FALSE );
		getch();
		exit(0);
	}
	else {
		mvwprintw( stdscr, 2, 0, "Save file created.");
	}
	refresh();
	mvwprintw( stdscr, 3, 0, "Populating save file..." );
	refresh();
	//Initialize database
	sql = "CREATE TABLE statistics(bits unsigned bigint, spacebars unsigned bigint, programmers unsigned bigint, bitfarms unsigned bigint, bitmines unsigned bigint, bitquant unsigned bigint, bitbang unsigned bigint, bitharmonizer unsigned bigint, bitgod unsigned bigint, bitcollider unsigned bigint);"
			"INSERT INTO statistics(bits, spacebars, programmers, bitfarms, bitmines, bitquant, bitbang, bitharmonizer, bitgod, bitcollider) values(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);"
			"CREATE TABLE time(hours unsigned bigint, minutes unsigned bigint, seconds unsigned bigint);"
			"INSERT INTO time(hours, minutes, seconds) values(0, 0, 0);";
	failed = sqlite3_exec( db, sql, NULL, 0, &errormsg );
	if ( failed != SQLITE_OK )
		mvwprintw( stdscr, 4, 0, "ERROR OCCURED: %s", errormsg );
	else
		mvwprintw( stdscr, 4, 0, "Finished making save file..." );
	refresh();
	sqlite3_close( db );
	usleep( 1000000 ); //Gives user enough time to see previous message.
	return;
}

void saveFunction() {
	clear();
	mvwprintw( stdscr, 0, 0, "Saving game..." );
	refresh();
	sqlite3_open( datapath, &db ); //Opens Database
	int failed;
	sqlite3_stmt *stmt;
	failed = sqlite3_prepare( db, "UPDATE statistics SET bits=?, spacebars=?, programmers=?, bitfarms=?, bitmines=?, bitquant=?, bitbang=?, bitharmonizer=?, bitgod=?, bitcollider=?", -1, &stmt, NULL ); //Sends sql command to read statistics table.
		if ( failed != SQLITE_OK )
			mvwprintw( stdscr, 1, 0, "ERROR OCCURED: %s", sqlite3_errmsg( db ) );
		else {
			mvwprintw( stdscr, 1, 0, "Saving stats..." );
			refresh();
			//Copies data from variables to their respective columns.
			sqlite3_bind_int64( stmt, 1, bits );
			sqlite3_bind_int64( stmt, 2, spacebars );
			sqlite3_bind_int64( stmt, 3, programmers );
			sqlite3_bind_int64( stmt, 4, bitfarms );
			sqlite3_bind_int64( stmt, 5, bitmines );
			sqlite3_bind_int64( stmt, 6, bitquant );
			sqlite3_bind_int64( stmt, 7, bitbang );
			sqlite3_bind_int64( stmt, 8, bitharmonizer );
			sqlite3_bind_int64( stmt, 9, bitgod );
			sqlite3_bind_int64( stmt, 10, bitcollider );
		}
	//Closes database
	sqlite3_step( stmt );
	sqlite3_finalize( stmt );
	mvwprintw( stdscr, 2, 0, "Finished saving stats, now saving time..." );
	failed = sqlite3_prepare( db, "UPDATE time SET hours=?, minutes=?, seconds=?", -1, &stmt, NULL ); //Sends sql command to read time table.
	if ( failed != SQLITE_OK )
		mvwprintw(stdscr, 3, 0, "ERROR OCCURED: %s", sqlite3_errmsg( db ) );
	else {
		sqlite3_bind_int64( stmt, 1, time[0] );
		sqlite3_bind_int64( stmt, 2, time[1] );
		sqlite3_bind_int64( stmt, 3, time[2] );
	}
	sqlite3_step( stmt );
	sqlite3_finalize( stmt );
	sqlite3_close( db );
	mvwprintw( stdscr, 4, 0, "Finished saving." );
	refresh();
	usleep( 1000000 ); //Gives user enough time to read previous message.
	return;
}

void loadFunction(){
	clear();
	mvwprintw( stdscr, 0, 0, "Loading save file..." );
	refresh();
	int failed = 0;
	sqlite3_open( datapath, &db ); //Opens database
	sqlite3_stmt *stmt;
	failed = sqlite3_prepare( db, "SELECT * FROM statistics;", -1, &stmt, NULL ); //Loads statistics table.
	sqlite3_step( stmt );
	if ( failed != SQLITE_OK )
		mvwprintw( stdscr, 1, 0, "ERROR OCCURED: %s", sqlite3_errmsg( db ) );
	else {
		mvwprintw( stdscr, 1, 0, "Loading stats..." );
		refresh();
		//Copies entries from table statistics to their variables
		bits = sqlite3_column_int64(stmt, 0);
		spacebars = sqlite3_column_int64(stmt, 1);
		programmers = sqlite3_column_int64(stmt, 2);
		bitfarms = sqlite3_column_int64(stmt, 3);
		bitmines = sqlite3_column_int64(stmt, 4);
		bitquant = sqlite3_column_int64(stmt, 5);
		bitbang = sqlite3_column_int64(stmt, 6);
		bitharmonizer = sqlite3_column_int64(stmt, 7);
		bitgod = sqlite3_column_int64(stmt, 8);
		bitcollider = sqlite3_column_int64(stmt, 9);
	}
	sqlite3_finalize(stmt);
	mvwprintw( stdscr, 2, 0, "Finished loading stats, loading time..." );
	failed = sqlite3_prepare( db, "SELECT * FROM time", -1, &stmt, NULL );
	sqlite3_step( stmt );
	if ( failed != SQLITE_OK )
		mvwprintw( stdscr, 3, 0, "ERROR OCCURED: %s", sqlite3_errmsg( db ) );
	else {
		time[0] = sqlite3_column_int64(stmt, 0);
		time[1] = sqlite3_column_int64(stmt, 1);
		time[2] = sqlite3_column_int64(stmt, 2);
	}
	sqlite3_finalize(stmt);
	sqlite3_close( db );
	mvwprintw( stdscr, 4, 0, "Finished loading." );
	refresh();
	usleep( 1000000 );
	return;
}

void controls(){
	keypress = getch();
	if (gameGoing == 1 && inMenu == 0){
		switch( toupper(keypress) ){
			case 'Q':
				endgame();
				break;
			case KEY_SPACE:
				bits++;
				break;
			case 'M':
				inMenu = 1;
				break;
			case 'H':
				helpScreen();
				break;
			default:
				break;
		}
	}
	else if (inMenu == 1){
		switch( toupper(keypress) ){
			case 'M':
				inMenu = 0;
				break;
			default:
				upgrades();
				break;
		}
	}
	return;
}

void helpScreen(){
	int8_t helpGoing = 1;
	int8_t scroll = 0;
	nodelay(stdscr, FALSE);
	while(helpGoing == 1){
		clear();
		mvprintw(scroll, 0, "HELP SCREEN FOR BITS\n");
		mvprintw(scroll + 2, 0,	   "In this game you press the spacebar to collect bits, you can spend those\n");
		mvprintw(scroll + 3, 0,    "bits to get upgrades in order to speed up your collection of bits. Your ultimate goal\n");
		mvprintw(scroll + 4, 0,    "is to reach the 64-bit integer, and when you do, you win the game!");
		mvprintw(scroll + 6, 0,    "Upgrades:");
		mvprintw(scroll + 7, 0,    "  Spacebars:");
		mvprintw(scroll + 8, 0,    "   This upgrade will automatically press the space bar every second.");
		mvprintw(scroll + 9, 0,    "  Programmers:");
		mvprintw(scroll + 10, 0,   "   These guys will work tirelessly to collect 10 bits a second.");
		mvprintw(scroll + 11, 0,   "  Bit Farms:");
		mvprintw(scroll + 12, 0,   "   A server farm that harvests 100 bits a second.");
		mvprintw(scroll + 13, 0,   "  Bit Mine:");
		mvprintw(scroll + 14, 0,   "   Using the power of the blockchain(tm), you can do stuff... I dunno, it gives 1,000 bits a second.");
		mvprintw(scroll + 15, 0,   "  Bit Quantumizer:");
		mvprintw(scroll + 16, 0,   "   Using experimental Quantum computing, you can harvest 10,000 bits a second.");
		mvprintw(scroll + 17, 0,   "  Artificial Bit Bang:");
		mvprintw(scroll + 18, 0,   "   By creating an artifical Bit Bang in a contain environment, you can harvest 100,000 bits a second.");
		mvprintw(scroll + 19, 0,   "  Bit Quantum Tunnel Harmonizer:");
		mvprintw(scroll + 20, 0,   "   I'm going to be honest, not even i'm sure how this works. All I know is that it");
		mvprintw(scroll + 21, 0,   "   pulls bit from multiple universes, bringing in 1,000,000 bits a second.");
		mvprintw(scroll + 22, 0,   "  Annual Sacrifices to the Bit God:");
		mvprintw(scroll + 23, 0,   "   We've made contact with a mysterious being who can create bits out of thin air.");
		mvprintw(scroll + 24, 0,   "   All he asks is for our undying obedience. In return he'll give up 100,000,000 bits a second.");
		mvprintw(scroll + 25, 0,   "  Bit Harmonic Energy Collider:");
		mvprintw(scroll + 26, 0,   "   We finally understand how the Bit God gets his bits, and using that same technology");
		mvprintw(scroll + 27, 0,   "   we can get even MORE bits, 1,000,000,000 to be exact per second.");
		mvprintw(scroll + 29, 0,   "This game is licensed under the GPL-3.0: https://www.gnu.org/licenses/gpl-3.0.en.html");
		mvprintw(scroll + 31, 0,   "Press Enter to return...");
		controls();
		switch(keypress){
			case KEY_DOWN:
				scroll--;
				break;
			case KEY_UP:
				scroll++;
				break;
			case KEY_RETURN:
				helpGoing = 0;
				break;
			default:
				break;
		}
		if (scroll == 1){
			scroll = 0;
		}
	}
	nodelay(stdscr, TRUE);
	clear();
	return;
}

void titlescreen(){
	int8_t titlescreenLoop = 1;
	int8_t menuSelected = 0;
	while (titlescreenLoop == 1){
		usleep(10000);
		attron(A_BOLD);
			mvprintw(0, 0, "Bits - A \"Clicker\" game.");
		attroff(A_BOLD);
		controls();
		switch (keypress){
			case KEY_DOWN:
				menuSelected++;
				break;
			case KEY_UP:
				menuSelected--;
				break;
			case KEY_RETURN:
				switch(menuSelected){
					case 0:
						saveFile();
						game();
						break;
					case 1:
						loadFunction();
						game();
						break;
					case 2:
						helpScreen();
						break;
					case 3:
						endgame();
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
		mvprintw(2, 0, " New Game");
		mvprintw(3, 0, " Load Game");
		mvprintw(4, 0, " Help");
		mvprintw(5, 0, " Quit");
		switch(menuSelected){
			case -1:
				menuSelected = 0;
				break;
			case 0:
				mvaddch(2, 0, '>');
				break;
			case 1:
				mvaddch(3, 0, '>');
				break;
			case 2:
				mvaddch(4, 0, '>');
				break;
			case 3:
				mvaddch(5, 0, '>');
				break;
			case 4:
				menuSelected = 3;
				break;
			default:
				break;
		}
	}
	return;
}

void initColors(){
	init_pair(1, COLOR_GREEN, COLOR_BLACK); //Bits
	init_pair(2, COLOR_BLUE, COLOR_BLACK); //Upgrades
	init_pair(3, COLOR_YELLOW, COLOR_BLACK); //Clock
	init_pair(4, COLOR_RED, COLOR_BLACK);
	return;
}

void endgame(){
	saveFunction();
	endwin();
	printf("Thanks for playing. This game was made as a way to test how math works in C, if you have any suggestions please open an issue on the gitlab page.");
	exit(0);
}

void renderUpgrades(){
	attron(COLOR_PAIR(UPGRADECOLOR));
		mvprintw(0, 40, "Spacebars:                      %i", spacebars);
		mvprintw(1, 40, "Programmers:                    %i", programmers);
		mvprintw(2, 40, "Bit Farms:                      %i", bitfarms);
		mvprintw(3, 40, "Bit Mines:                      %i", bitmines);
		mvprintw(4, 40, "Bit Quantumizers:               %i", bitquant);
		mvprintw(5, 40, "Bit Bang Generators:            %i", bitbang);
		mvprintw(6, 40, "Bit Quantum Tunnel Harmonizers: %i", bitharmonizer);
		mvprintw(7, 40, "Yearly Sacrifices to Bit God:   %i", bitgod);
		mvprintw(8, 40, "Bit Harmonic Energy Colliders:  %i", bitcollider);
	attroff(COLOR_PAIR(UPGRADECOLOR));
	return;
}

void game(){
	clear();
	gameGoing = 1;
	while (gameGoing == 1){
		usleep(10000);
		if (bits <= -1){
			bits = 9223372036854775807;
		}
		renderUpgrades();
		timer();
		controls();
		bitsCalc();
		tick();
		attron(COLOR_PAIR(REDCOLOR));
			mvprintw(9, 0, "Press M to open shop. Press Q to quit game.");
		attroff(COLOR_PAIR(REDCOLOR));
		if (bits == 0){
			mvprintw(10, 0, "<-- PRESS SPACE BAR TO COLLECT BITS -->");
		}
		else if (bits == 1){
			mvprintw(10, 0, "                                       ");
		}
		if (inMenu == 1){
			menu();
		}
	}
	return;
}

void timer(){
	if (ticks % 100 == 0){
		time[2]++;
		if ( time[2] == 60 ) {
			time[2] = 0;
			time[1]++;
		}
		if ( time[1] == 60 ) {
			time[1] = 0;
			time[0]++;
		}
	}
	attron( COLOR_PAIR(3) );
		mvprintw( 0, 0, "Timer: %i:%02i:%02i", time[0], time[1], time[2] );
	attroff( COLOR_PAIR(3) );
	return;
}

void bitsCalc(){
	if (ticks % 100 == 0){
		bits += 1 * spacebars;
		bits += 10 * programmers;
		bits += 100 * bitfarms;
		bits += 1000 * bitmines;
		bits += 10000 * bitquant;
		bits += 100000 * bitbang;
		bits += 1000000 * bitharmonizer;
		bits += 100000000 * bitgod;
		bits += 1000000000 * bitcollider;
	}
	attron(COLOR_PAIR(1));
	attron(A_BOLD);
		mvprintw(1, 0, "Bits: %li", bits);
	attroff(A_BOLD);
	attroff(COLOR_PAIR(1));
	return;
}

void tick(){
	ticks++;
	return;
}

void menu(){
	nodelay(stdscr, FALSE);
	clear();
	while (inMenu == 1){
		attron(COLOR_PAIR(REDCOLOR));
			mvprintw(10, 0,  "Press M to exit shop.");
		attroff(COLOR_PAIR(REDCOLOR));
		mvprintw(11, 0,  "                                ");
		attron(COLOR_PAIR(BITCOLOR));
		attron(A_BOLD);
			mvprintw(11, 0,  "Bits: %lli", bits);
		attroff(A_BOLD);
		attroff(COLOR_PAIR(BITCOLOR));
		attron(COLOR_PAIR(UPGRADECOLOR));
		attron(A_BOLD);
			mvprintw(0, 0,  "01. Space Bars                    - 100 bits");
			mvprintw(1, 0, "02. Programmers                   - 1,000 bits");
			mvprintw(2, 0,  "03. Bit Farms                     - 100,000 bits");
			mvprintw(3, 0, "04. Bit Mines                     - 1,000,000 bits");
			mvprintw(4, 0,  "05. Bit Quantumizer               - 10,000,000 bits");
			mvprintw(5, 0, "06. Bit Bang Generator            - 100,000,000 bits");
			mvprintw(6, 0,  "07. Bit Quantum Tunnel Harmonizer - 1,000,000,000 bits");
			mvprintw(7, 0, "08. Annual Sacrifices to Bit God  - 10,000,000,000 bits");
			mvprintw(8, 0,  "09. Bit Harmonic Energy Collider  - 100,000,000,000 bits");
			mvprintw(9, 0, "00. Achieve Bit Godhood           - 9,223,372,036,854,775,807 bits");
		attroff(A_BOLD);
		attroff(COLOR_PAIR(UPGRADECOLOR));
		controls();
	}
	nodelay(stdscr, TRUE);
	clear();
	return;
}

void upgrades(){
	clear();
	switch( toupper(keypress) ){
		case '1':
			if (bits >= 100){
				bits -= 100;
				spacebars++;
				mvprintw(12, 0, "<-- YOU BOUGHT A SPACE BAR -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '2':
			if (bits >= 1000){
				bits -= 1000;
				programmers++;
				mvprintw(12, 0, "<-- YOU BOUGHT A PROGRAMMER -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '3':
			if (bits >= 100000){
				bits -= 100000;
				bitfarms++;
				mvprintw(12, 0, "<-- YOU BOUGHT A BIT FARM -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '4':
			if (bits >= 1000000){
				bits -= 1000000;
				bitmines++;
				mvprintw(12, 0, "<-- YOU BOUGHT A BIT MINE -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '5':
			if (bits >= 10000000){
				bits -= 10000000;
				bitquant++;
				mvprintw(12, 0, "<-- YOU BOUGHT A BIT QUANTUMIZER -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '6':
			if (bits >= 100000000){
				bits -= 100000000;
				bitbang++;
				mvprintw(12, 0, "<-- YOU BOUGHT AN ARTIFICIAL BIG BANG GENERATOR -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '7':
			if (bits >= 1000000000){
				bits -=  1000000000;
				bitharmonizer++;
				mvprintw(12, 0, "<-- YOU BOUGHT A BIT QUANTUM TUNNEL HARMONIZER -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '8':
			if (bits >= 10000000000){
				bits -=  10000000000;
				bitgod++;
				mvprintw(12, 0, "<-- YOU ARE NOW FUNDING ANNUAL SACRIFICES TO THE BIT GOD -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '9':
			if (bits >= 100000000000){
				bits -=  100000000000;
				bitcollider++;
				mvprintw(12, 0, "<-- YOU BOUGHT A BIT HARMONIC ENERGY COLLIDER -->");
			}
			else{
				mvprintw(12, 0, "<-- YOU DON'T HAVE ENOUGH BITS -->");
			}
			break;
		case '0':
			if (bits == 9223372036854775807){
				endgame();
			}
			else{
				mvprintw(12, 0, "<-- NOT YET -->");
			}
		default:
			break;
	}
	return;
}
