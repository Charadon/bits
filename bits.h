#ifndef BITS_H
#define BITS_H
	#include "stdio.h"
	#include "stdlib.h"
	#include "curses.h"
	#include "stdint.h"
	#include "unistd.h"
	#include "ctype.h"
	#include "string.h"
	#include "sqlite3.h"
	#include "sys/stat.h"
	#include "sys/types.h"
	#define KEY_RETURN 10
	#define KEY_SPACE 32
	void titlescreen();
	void initColors();
	void endgame();
	void controls();
	void helpScreen();
	void timer();
	void game();
	void bitsCalc();
	void tick();
	void menu();
	void upgrades();
	void renderUpgrades();
	void saveFile();
	void saveFunction();
	void loadFunction();
#endif
