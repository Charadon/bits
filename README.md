This game was created as a way to figure out how math, and file i/o worked. I actually want to detail my journey here.

So, this started when I was wanting to figure out the extend of how math works in C. I 
was watching a video about cookie clicker and I said to myself "Yeah, I can make something 
like that..." and so I began work. I designed the game so you win when the bits integer 
rolls into the negatives ( which implies an overflow, thus you reached the limit. )

After experimenting with math a bit, I realized that I need to create a save file, and this 
was the most infuriating thing i've done in C so far. At first, I tried 
json, unfortunately, I found that tutorials for using json in C was 
lacking, so next I tried XML, and after jumping from xml library to the other, I gave up 
on that because parsing was extremely difficult. After XML, I gave up for a while and 
worked on dungeon some more. Later, I found SQLite, and realized that it was literally an 
SQL database, but in a single file! And after reading some basic SQL tutorials, and reading 
the documentation for SQLite C functions, I had an actual working save feature. It isn't 
perfect right now, as I haven't coded in certain error detections, but it works, 
and works pretty damn well.

After creating the save feature, I then wanted to follow certain standards. On Unix-like, I
wanted to follow the freedesktop standard, and I found it pretty easy. And likewise, on Windows
( because Windows is a very special & needy boi ) it was pretty easy. All I had to do was grab
their respective environment variables ( HOME on Unix, APPDATA on Windows ) and then create a
folder called "bits" and dump the SQLite database in there. Easy. It was also the first time
I had to use #ifdef, because I wanted certain instructions to run on Unix, and certain instructions
to run on Windows.

Anyways, that's pretty much it when it comes to this project. You can use it to study if you want, but
I wouldn't recommend it because my code in this was spaghetti.